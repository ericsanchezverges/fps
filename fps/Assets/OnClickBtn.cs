﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class OnClickBtn : MonoBehaviour
{
    public Button playGame, exitGame;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        playGame.onClick.AddListener(PlayGame);
        exitGame.onClick.AddListener(ExitGame);
    }

    // Update is called once per frame
  
    void PlayGame()
    {
        SceneManager.LoadScene("MainGame", LoadSceneMode.Single);
    }

    void ExitGame()
    {
        Application.Quit();
    }
}
