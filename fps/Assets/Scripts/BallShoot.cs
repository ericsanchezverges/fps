using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BallShoot : MonoBehaviour {
	
	public GameObject bullet_prefab;
	public float bulletImpulse = 5f;
    bool usingRayGun, invul = false;
    GameObject rayParticle;
    public ParticleSystem gunParticle;
    ParticleSystem.MainModule main;
    public int laserDMG;
    public LayerMask mask;
    public Image laserAmmoImg;
    public float laserAmmo, speedAmmoLoss;

    private void Start()
    {
        rayParticle = GameObject.Find("RayParticle");
        rayParticle.SetActive(false);
        main = gunParticle.main;
    }
    // Update is called once per frame
    public void Shot () {
        if (usingRayGun) return;
		GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
		thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (usingRayGun)
            {
                Debug.Log("Swap to balls");
                usingRayGun = false;
            }
            else
            {
                Debug.Log("Swap to lightning");
                usingRayGun = true;
            }
        }

        if (Input.GetKey(KeyCode.Mouse0) && usingRayGun && laserAmmo > 0)
        {
           
            rayParticle.SetActive(true);
            main.simulationSpeed = 50f;
            laserAmmo -= speedAmmoLoss * Time.deltaTime;
            laserAmmoImg.fillAmount = laserAmmo / 100;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity,mask))
            {
                if (hit.collider.tag == "Enemy" && hit.collider.GetType() == typeof(CapsuleCollider))
                {
                    if (!invul)
                    {
                        ZombieController zc = hit.collider.gameObject.GetComponent<ZombieController>();
                        zc.life -= laserDMG;
                        zc.m_animator.SetBool("isShoot", true);
                        zc.shootCooldown = 0.1f;
                        Debug.Log("Attacking zombie");
                        StartCoroutine(makeDmg());
                    }
                }
            }
        }
        else
        {
            rayParticle.SetActive(false);
            main.simulationSpeed = 2f;   
        }
        if (laserAmmo > 100) laserAmmo = 100;
    }

    IEnumerator makeDmg()
    {
        invul = true;
        yield return new WaitForSeconds(0.1f);
        invul = false;
    }
}
