﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class ZombieController : MonoBehaviour {
    public int life;
    [HideInInspector] public bool isMoving, isAlive, isEating, isShoot, isChasing;
    [HideInInspector] public float shootCooldown;
    [HideInInspector] public Animator m_animator;
    public Vector3[] patrolPoints;
    [HideInInspector] public NavMeshAgent navAgent;
    int aux = 0;
    public float chaseVel;
    public CapsuleCollider collider, trigger;
    Transform playerTf;
    public GameObject ammo;
    
    // Use this for initialization
    void Start () {
        isChasing = isEating = isShoot = false;
        isAlive = isMoving = true;

        m_animator = GetComponent<Animator>();
        m_animator.SetBool("isAlive", isAlive);
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.SetDestination(patrolPoints[aux]);
        playerTf = GameObject.Find("Player").GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!isAlive) return;
        setupAnimatorVar();
        if (shootCooldown <= 0) m_animator.SetBool("isShoot", false);
        patrolling();
        if(life <= 0)
        {
            isAlive = false;
            m_animator.SetBool("isAlive", isAlive);
            collider.enabled = trigger.enabled = false;
            navAgent.SetDestination(transform.position);
            StartCoroutine(deleteZombie());
            GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
            Instantiate(ammo, GetComponent<Transform>());
        }
    }

    private void FixedUpdate()
    {
        shootCooldown -= Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Shot")
        {
            life -= 25;
            m_animator.SetBool("isShoot", true);
            shootCooldown = 0.2f;
        }
    }

    void patrolling()
    {
        if (isChasing)
        {
            navAgent.SetDestination(playerTf.position);
            return;
        }
        else navAgent.SetDestination(patrolPoints[aux]);
        if (transform.position.x == patrolPoints[aux].x || transform.position.z == patrolPoints[aux].z)
        {
            for (int i = aux; i < patrolPoints.Length; i++)
            {
                navAgent.SetDestination(patrolPoints[i]);
                aux++;
                if (aux == patrolPoints.Length) aux = 0;
                return;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        for (int i = 0; i < patrolPoints.Length; i++) {
            Gizmos.color = new Color(1, 0, 0);
            Gizmos.DrawCube(patrolPoints[i], new Vector3(0.3f, 0.3f, 0.3f));
            if(i > 0)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(patrolPoints[i], patrolPoints[i - 1]);
            }

        }
    }

    

    void setupAnimatorVar()
    {
        m_animator.SetBool("isMoving", isMoving);
    }

    IEnumerator deleteZombie()
    {
        yield return new WaitForSeconds(30f);
        Destroy(this.gameObject);
    }


}
