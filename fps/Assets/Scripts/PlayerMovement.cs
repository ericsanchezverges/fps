﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	private CharacterController controller;
	private Vector2 axis;
	public float speed;
	public Vector3 moveDirection;
	private float forceToGround = Physics.gravity.y;
    private BallShoot bs;

	public float jumpSpeed;
	private bool jump, invul = false;
	public float gravityMagnitude = 1.0f;
    public float health;
    Image healthBar;

	void Start ()
	{
		controller = GetComponent<CharacterController>();
        healthBar = GameObject.Find("LifeBar").GetComponent<Image>();
        bs = GetComponent<BallShoot>();
    }

	void Update ()
	{
		if(controller.isGrounded && !jump)
		{
			moveDirection.y = forceToGround;
		}
		else
		{
			jump = false;
			moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
		}

		Vector3 transformDirection = axis.x * transform.right + axis.y * transform.forward;

		moveDirection.x = transformDirection.x * speed;
		moveDirection.z = transformDirection.z * speed;

		controller.Move(moveDirection * Time.deltaTime);

        if (health <= 0)
        {
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }//PlayerDies
    }

	public void SetAxis(Vector2 inputAxis)
	{
		axis = inputAxis;
	}

	public void StartJump()
	{
		if(!controller.isGrounded) return;

		moveDirection.y = jumpSpeed;
		jump = true;
	}

    
    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Enemy" && col.GetType() == typeof(CapsuleCollider))
        {
            if (!invul)
            {
                health -= 10;
                healthBar.fillAmount = health / 100;
                StartCoroutine(dmgTaken());
            }
        }
    }

    IEnumerator dmgTaken()
    {
        invul = true;
        yield return new WaitForSeconds(2);
        invul = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ammo")
        {
            bs.laserAmmo += 10;
            bs.laserAmmoImg.fillAmount = bs.laserAmmo / 100;
            Destroy(other.gameObject);
        }
    }

}
