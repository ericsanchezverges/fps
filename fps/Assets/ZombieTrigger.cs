﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieTrigger : MonoBehaviour
{
    public ZombieController zc;

    private void Start()
    {
        zc = GetComponentInParent<ZombieController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            zc.isChasing = true;
            zc.m_animator.SetBool("isChasing", zc.isChasing);
            zc.navAgent.speed = zc.chaseVel;
        }
    }
}
